from abc import abstractmethod
import random
from .exceptions import *


class Army:
  
    def __init__(self, legion, gold=0):
        self.__gold = gold
        self.__legion = legion

    def attack(self, army):
        return army.attack_with_points(self, self.power)

    def attack_with_points(self, attacker, power_received):
        winner = None
        if self.power < power_received:
            attacker.win()
            self.lose()
            winner = attacker
        elif self.power > power_received:
            self.win()
            attacker.lose()
            winner = self
        else:
            self.draw()
            attacker.draw()
        return winner


    def win(self):
        self.__gold += 100

    def lose(self):
        for _ in range(2):
            if len(self.__legion) == 0:
                raise NoSoldiersToKillException("Your legion is empty")
            else:
                doomed = max(self.__legion, key=lambda x: x.points)
                self.__legion.remove(doomed)

    def select_witch_soldier_dies(self):
        return random.choice(self.__legion)

    def draw(self):
        to_be_removed = self.select_witch_soldier_dies()
        self.__legion.remove(to_be_removed)

    def substract_gold(self, amount):
        if self.__gold >= amount:
            self.__gold -= amount
        else:
            raise NotEnoughtGoldException(
                "There is not enough gold for operation")

    @property
    def gold_stored(self):
        return self.__gold

    @property
    def power(self):
        return sum((soldier.points for soldier in self.__legion), 0)


class Soldier:

    def __init__(self, points):
        self._points = points

    def upgrade(self, owner):
        owner.substract_gold(self.upgrade_cost())
        self._points += self.upgrade_points()

    @abstractmethod
    def upgrade_cost(self):
        pass

    @abstractmethod
    def upgrade_points(self):
        pass

    @abstractmethod
    def transform_cost(self):
        pass

    @abstractmethod
    def transform_result_class(self):
        pass

    def transform(self, owner):
        owner.substract_gold(self.transform_cost())
        return self.transform_result_class()


class Pikeman(Soldier):

    def __init__(self):
        super().__init__(5)

    @property
    def points(self):
        return self._points

    def upgrade_cost(self):
        return 3

    def upgrade_points(self):
        return 10

    def transform_cost(self):
        return 30

    def transform_result_class(self):
        return Bowman()


class Bowman(Soldier):

    def __init__(self):
        super().__init__(10)


    @property
    def points(self):
        return self._points

    def upgrade_cost(self):
        return 7

    def upgrade_points(self):
        return 20

    def transform_cost(self):
        return 40

    def transform_result_class(self):
        return Knight()


class Knight(Soldier):

    def __init__(self):
        super().__init__(20)

    @property
    def points(self):
        return self._points

    def upgrade_cost(self):
        return 10

    def upgrade_points(self):
        return 30

    def transform(self, owner):
        raise UnitInvalidTransformationException(
            "Cannot transform from knight to other unit")
