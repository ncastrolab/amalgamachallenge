from amalgama_challenge.army import *

class ArmiesFactory:

    @staticmethod
    def create_army(pikes, bows, horses):

        legion = []
        for _ in range(pikes):
            legion.append(Pikeman())

        for _ in range(bows):
            legion.append(Bowman())

        for _ in range(horses):
            legion.append(Knight())

        return Army(legion)
    
    @staticmethod
    def create_chinese():
        return ArmiesFactory.create_army(2, 25, 2)

    @staticmethod
    def create_british():
        return ArmiesFactory.create_army(10, 10, 10)

    @staticmethod
    def create_byzantines():
        return ArmiesFactory.create_army(5, 8, 15)