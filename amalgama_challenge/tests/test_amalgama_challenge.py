import pytest

from amalgama_challenge import __version__
from amalgama_challenge.army import *
from amalgama_challenge.factories import ArmiesFactory
from unittest.mock import MagicMock

def test_version():
    assert __version__ == '0.1.0'

def test_pikeman_propertly_setted():
    pikeman = Pikeman()
    assert pikeman.points == 5

def test_bowman_propertly_setted():
    bowman = Bowman()
    assert bowman.points == 10

def test_bowman_propertly_setted():
    knight = Knight()
    assert knight.points == 20

def test_army_unit_has_always_points():
    knight = Knight()
    bowman = Bowman()
    pikeman = Pikeman()
    assert knight.points is not None
    assert bowman.points is not None
    assert pikeman.points is not None

def test_pikeman_training_ups_expected_points():
    pikeman = Pikeman()
    test_army = Army([pikeman], 3)

    assert pikeman.points == 5
    pikeman.upgrade(test_army)
    assert pikeman.points == 15
    assert test_army.gold_stored == 0

def test_bowman_training_ups_expected_points():
    bowman = Bowman()
    assert bowman.points == 10
    test_army = Army([bowman], 7)
    bowman.upgrade(test_army)
    assert bowman.points == 30
    assert test_army.gold_stored == 0

def test_knight_training_ups_expected_points():
    knight = Knight()
    assert knight.points == 20
    test_army = Army([knight], 10)
    knight.upgrade(test_army)
    assert knight.points == 50
    assert test_army.gold_stored == 0

def test_pikeman_transform_to_bowman():
    pikeman = Pikeman()
    test_army = Army([pikeman], 30)
    bowman = pikeman.transform(test_army)
    assert bowman.points == 10
    assert type(bowman) == Bowman

def test_bowman_transform_to_knight():
    bowman = Bowman()
    test_army = Army([bowman], 40)
    knight = bowman.transform(test_army)
    assert knight.points == 20
    assert type(knight) == Knight

def test_knight_transform_raise_exception():
    knight = Knight()
    test_army = Army([knight], 8000)
    with pytest.raises(UnitInvalidTransformationException) as e_info:
        knight.transform(test_army)
        assert e_info.message == "Cannot transform from knight to other unit"

def test_army_with_more_sum_of_points_result_winner_and_get_100_coins():
    chinese = ArmiesFactory.create_chinese()
    british = ArmiesFactory.create_british()
    byzantine = ArmiesFactory.create_byzantines()

    assert chinese.gold_stored == 0
    assert british.gold_stored == 0
    assert byzantine.gold_stored == 0

    winner = british.attack(chinese)
    assert winner == british

    assert chinese.gold_stored == 0
    assert british.gold_stored == 100
    assert byzantine.gold_stored == 0

def test_army_that_lose_get_the_two_more_valuable_units_removed():
    chinese = ArmiesFactory.create_chinese()
    british = ArmiesFactory.create_british()
    byzantine = ArmiesFactory.create_byzantines()

    assert chinese.power == 300
    assert british.power == 350
    assert byzantine.power == 405

    winner = british.attack(chinese)
    assert winner == british

    assert chinese.power == 260
    assert british.power == 350
    assert byzantine.power == 405

def test_armies_draw_result_get_each_army_with_one_unit_removed():
    pikeman = Pikeman()
    bowman = Bowman()
    knight = Knight()

    pikemanTwo = Pikeman()
    bowmanTwo = Bowman()
    knightTwo = Knight()

    eastChinese = Army([pikeman, bowman, knight])
    westChinese = Army([pikemanTwo, bowmanTwo, knightTwo])

    eastChinese.select_witch_soldier_dies = MagicMock(return_value=pikeman)
    westChinese.select_witch_soldier_dies = MagicMock(return_value=pikemanTwo)

    assert eastChinese.power == 35
    assert westChinese.power == 35

    winner = eastChinese.attack(westChinese)
    assert winner == None

    assert eastChinese.power == 30
    assert eastChinese.power == 30
